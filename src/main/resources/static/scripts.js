document.addEventListener("DOMContentLoaded", function () {
    const categoriesContainer = document.getElementById('categories');
    const similarProductList = document.getElementById('similar-product-list');

    let selectedProductId = null; // Variable to store the ID of the selected product

    // Function to fetch all categories and their products
    function fetchAllCategories() {
        fetch('/api/category/get/all')
            .then(response => response.json())
            .then(data => {
                categoriesContainer.innerHTML = ''; // Clear previous categories
                data.forEach(category => {
                    const categoryHeading = document.createElement('h2');
                    categoryHeading.textContent = category.name;
                    categoriesContainer.appendChild(categoryHeading);

                    const productList = document.createElement('ul');
                    category.products.forEach(product => {
                        const li = document.createElement('li');
                        li.classList.add('product-item');
                        li.id = `product-${product.id}`; // Assign an ID to each product item
                        li.textContent = `${product.name} - $${product.price} - ${product.description}`;
                        const selectButton = document.createElement('button');
                        selectButton.textContent = 'Select';
                        selectButton.onclick = function() {
                            // Highlight the selected product
                            selectProduct(product.id);
                        };
                        li.appendChild(selectButton);
                        productList.appendChild(li);
                    });

                    categoriesContainer.appendChild(productList);
                });
            });
    }

    // Function to select a product
    function selectProduct(productId) {
        // Highlight the selected product
        highlightSelectedProduct(productId);

        // Fetch similar products from the same category
        fetchSimilarProducts(productId);
    }

    // Function to highlight the selected product
    function highlightSelectedProduct(productId) {
        // Remove highlighting from previously selected product, if any
        const previousSelectedProduct = document.querySelector('.selected-product');
        if (previousSelectedProduct) {
            previousSelectedProduct.classList.remove('selected-product');
        }

        // Highlight the newly selected product
        const selectedProduct = document.getElementById(`product-${productId}`);
        if (selectedProduct) {
            selectedProduct.classList.add('selected-product');
        }

        // Store the ID of the selected product
        selectedProductId = productId;
    }

    // Function to fetch similar products from the same category
    function fetchSimilarProducts(productId) {
        fetch(`/api/product/get/similar/${productId}`)
            .then(response => response.json())
            .then(data => {
                similarProductList.innerHTML = ''; // Clear previous similar products
                data.forEach(product => {
                    // Exclude the selected product from the similar products list
                    if (product.id !== selectedProductId) {
                        const li = document.createElement('li');
                        li.classList.add('product-item');
                        li.textContent = `${product.name} - $${product.price} - ${product.description}`;
                        similarProductList.appendChild(li);
                    }
                });
            });
    }

    // Fetch all categories and their products when the page loads
    fetchAllCategories();
});
