package com.product.ecommerce.service;

import java.util.List;
import java.util.Optional;

import com.product.ecommerce.model.Product;

public interface ProductService {
	
	public Optional<Product> getOneProduct(Long id);
	
	public List<Product> getAllProducts();
	
	public Product createProduct(Product newProduct);
	
	public Product updateProduct(Product updatedProduct,Long id);
	
	public void deleteProduct(Long id);
	

	public List<Product> getSimilarProducts(Long productId);
}
