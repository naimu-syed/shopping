package com.product.ecommerce.service.impls;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.product.ecommerce.model.Product;
import com.product.ecommerce.repositories.ProductRepo;
import com.product.ecommerce.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	public ProductRepo productRepo;
	
	@Override
	public Optional<Product> getOneProduct(Long id) {
		return productRepo.findById(id);
	}

	@Override
	public List<Product> getAllProducts() {
		return productRepo.findAll();	}

	@Override
	public Product createProduct(Product newProduct) {
		return productRepo.save(newProduct);
	}

	@Override
	public Product updateProduct(Product updatedProduct, Long id) {
		
		Product existingProduct = productRepo.findById(id).orElseThrow();
		existingProduct.setId(existingProduct.getId());
		existingProduct.setName(existingProduct.getName());
		existingProduct.setPrice(existingProduct.getPrice());
		existingProduct.setDescription(existingProduct.getDescription());
		
		return productRepo.save(updatedProduct);
	}

	@Override
	public void deleteProduct(Long id) {
		productRepo.deleteById(id);
		
	}

	@Override
	public List<Product> getSimilarProducts(Long productId) {
	    Product product = productRepo.findById(productId).orElseThrow();
	    if (product == null) {
	        return new ArrayList<>(); // Return an empty list if product not found
	    }

	    Long categoryId = product.getCategory().getId();
	    List<Product> productsInCategory = productRepo.findByCategoryId(categoryId);

	    List<Product> similarProducts = new ArrayList<>();
	    for (int i = 0; i < productsInCategory.size(); i++) {
	        Product p = productsInCategory.get(i);
	        if (!p.getId().equals(productId)) {
	            similarProducts.add(p);
	        }
	    }
	    
	    return similarProducts;
	}



}
