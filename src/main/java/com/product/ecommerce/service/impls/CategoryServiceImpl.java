package com.product.ecommerce.service.impls;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.product.ecommerce.model.Category;
import com.product.ecommerce.repositories.CategoryRepo;
import com.product.ecommerce.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepo categoryRepo;
	
	@Override
	public Optional<Category> getOneCategory(Long id) {
		return categoryRepo.findById(id);
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryRepo.findAll();
	}

	@Override
	public Category createCategory(Category newCategory) {
		return categoryRepo.save(newCategory);
	}

	@Override
	public Category updateCategory(Category updatedCategory, Long id) {
		Category existingCategory = categoryRepo.findById(id).orElseThrow();
		existingCategory.setId(existingCategory.getId());
		existingCategory.setName(existingCategory.getName());
		existingCategory.setDescription(existingCategory.getDescription());
		
		return categoryRepo.save(updatedCategory);
	}

	@Override
	public void deleteCategory(Long id) {
		categoryRepo.deleteById(id);
	}

	@Override
	public List<Category> getCategoriesByProductId(Long productId) {
		return categoryRepo.findByProducts_Id(productId);
	}

}
