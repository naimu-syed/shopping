package com.product.ecommerce.service;

import java.util.List;
import java.util.Optional;

import com.product.ecommerce.model.Category;

public interface CategoryService {
	
	public Optional<Category> getOneCategory(Long id);
	
	public List<Category> getAllCategories();
	
	public Category createCategory(Category newCategory);
	
	public Category updateCategory(Category updatedCategory,Long id);
	
	public void deleteCategory(Long id);
	
	public List<Category> getCategoriesByProductId(Long productId);

}
