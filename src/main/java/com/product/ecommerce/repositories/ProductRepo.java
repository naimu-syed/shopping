package com.product.ecommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.ecommerce.model.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {
	

	List<Product> findByCategoryId(Long categoryId);

}
