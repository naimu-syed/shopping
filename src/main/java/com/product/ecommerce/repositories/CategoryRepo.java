package com.product.ecommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.product.ecommerce.model.Category;

@Repository
public interface CategoryRepo extends JpaRepository<Category, Long>{
	List<Category> findByProducts_Id(Long productId);
	

}
