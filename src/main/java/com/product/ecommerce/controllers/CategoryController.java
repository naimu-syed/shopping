package com.product.ecommerce.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.product.ecommerce.model.Category;
import com.product.ecommerce.service.impls.CategoryServiceImpl;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

	@Autowired
	private CategoryServiceImpl categoryService;
	
	@GetMapping("/{id}")
	public Optional<Category> getCategoryById(@PathVariable Long id){
		return categoryService.getOneCategory(id);	
	}
	
	@GetMapping("/get/all")
	public List<Category> getListOfCategories(){
		return categoryService.getAllCategories();
	}
	
	@PostMapping("/create")
	public Category createNewCategory(@RequestBody Category newCategory) {
		return categoryService.createCategory(newCategory);
	}
	
	@PutMapping("/update/{id}")
	public Category updatCategory(@RequestBody Category updatCategory,@PathVariable Long id) {
		return categoryService.updateCategory(updatCategory, id);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteCategoryById(@PathVariable Long id) {
		categoryService.deleteCategory(id);
	}
	
	@GetMapping("/get/similar/{productId}")
    public List<Category> getCategoriesByProductId(@PathVariable Long productId) {
        return categoryService.getCategoriesByProductId(productId);
    }
}
