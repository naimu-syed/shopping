package com.product.ecommerce.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.product.ecommerce.model.Product;
import com.product.ecommerce.service.impls.ProductServiceImpl;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	private ProductServiceImpl productService;

	@GetMapping("/{id}")
	public Optional<Product> getProductById(@PathVariable Long id) {
		return productService.getOneProduct(id);
	}
	
	@GetMapping("/get/all")
	public List<Product> getListOfProducts(){
		return productService.getAllProducts();
	}
	
	@PostMapping("/create")
	public Product createNewProduct(@RequestBody Product newProduct) {
		return productService.createProduct(newProduct);
	}
	
	@PutMapping("/update/{id}")
	public Product updatProduct(@RequestBody Product updatedProduct,@PathVariable Long id) {
		return productService.updateProduct(updatedProduct, id);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deleteProduct(@PathVariable Long id) {
		productService.deleteProduct(id);
	}
	@GetMapping("/get/similar/{productId}")
	public List<Product> getSimilarProducts(@PathVariable Long productId) {
	    return productService.getSimilarProducts(productId);
	}

}
