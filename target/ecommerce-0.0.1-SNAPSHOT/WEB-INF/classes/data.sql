-- Script for creating the category table

CREATE TABLE IF NOT EXISTS category (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(255) NOT NULL
);

-- Script for creating the product table
CREATE TABLE IF NOT EXISTS product (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    description VARCHAR(255) NOT NULL,
    category_id INT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(id)
);
-- Insert sample categories with descriptions
INSERT INTO category (id, name, description) VALUES (1, 'Clothing', 'Explore our collection of trendy clothing for all occasions.');
INSERT INTO category (id, name, description) VALUES (2, 'Electronics', 'Discover the latest gadgets and electronics to simplify your life.');
INSERT INTO category (id, name, description) VALUES (3, 'Footwear', 'Step out in style with our range of comfortable and fashionable footwear.');
INSERT INTO category (id, name, description) VALUES (4, 'Accessories', 'Accessorize your look with our stunning selection of jewelry, bags, and more.');

-- Insert sample products with descriptions and prices for Clothing category
INSERT INTO product (id, name, price, description, category_id) VALUES (1, 'Cotton T-shirt', 20.0, 'Stay comfortable all day long in our soft cotton T-shirt.', 1);
INSERT INTO product (id, name, price, description, category_id) VALUES (2, 'Blue Jeans', 30.0, 'Classic blue jeans for a casual yet stylish look.', 1);
INSERT INTO product (id, name, price, description, category_id) VALUES (3, 'Hoodie Sweatshirt', 40.0, 'Keep warm and cozy in our stylish hoodie sweatshirt.', 1);
INSERT INTO product (id, name, price, description, category_id) VALUES (4, 'Formal Shirt', 25.0, 'Look sharp and sophisticated in our tailored formal shirt.', 1);
INSERT INTO product (id, name, price, description, category_id) VALUES (5, 'Summer Dress', 35.0, 'Flaunt your style with our chic and breezy summer dress.', 1);

-- Insert sample products with descriptions and prices for Electronics category
INSERT INTO product (id, name, price, description, category_id) VALUES (6, 'Smartphone', 500.0, 'Experience the latest technology with our high-performance smartphone.', 2);
INSERT INTO product (id, name, price, description, category_id) VALUES (7, 'Laptop', 1000.0, 'Powerful laptop for work and entertainment on the go.', 2);
INSERT INTO product (id, name, price, description, category_id) VALUES (8, 'Wireless Earbuds', 70.0, 'Enjoy crisp sound and ultimate convenience with our wireless earbuds.', 2);
INSERT INTO product (id, name, price, description, category_id) VALUES (9, 'Bluetooth Speaker', 50.0, 'Bring the party anywhere with our portable Bluetooth speaker.', 2);
INSERT INTO product (id, name, price, description, category_id) VALUES (10, 'Smartwatch', 150.0, 'Stay connected and track your fitness goals with our smartwatch.', 2);

-- Insert sample products with descriptions and prices for Footwear category
INSERT INTO product (id, name, price, description, category_id) VALUES (11, 'Running Shoes', 50.0, 'Get active with our comfortable and durable running shoes.', 3);
INSERT INTO product (id, name, price, description, category_id) VALUES (12, 'Summer Sandals', 30.0, 'Stay cool and stylish this summer with our trendy sandals.', 3);
INSERT INTO product (id, name, price, description, category_id) VALUES (13, 'Sneakers', 40.0, 'Step up your street style with our trendy sneakers.', 3);
INSERT INTO product (id, name, price, description, category_id) VALUES (14, 'Winter Boots', 60.0, 'Stay warm and stylish during the cold season with our cozy winter boots.', 3);
INSERT INTO product (id, name, price, description, category_id) VALUES (15, 'High Heels', 45.0, 'Elevate your look with our chic and elegant high heels.', 3);

-- Insert sample products with descriptions and prices for Accessories category
INSERT INTO product (id, name, price, description, category_id) VALUES (16, 'Travel Backpack', 40.0, 'Travel in style with our spacious and practical backpack.', 4);
INSERT INTO product (id, name, price, description, category_id) VALUES (17, 'Luxury Watch', 100.0, 'Add a touch of elegance to any outfit with our luxurious watch.', 4);
INSERT INTO product (id, name, price, description, category_id) VALUES (18, 'Designer Sunglasses', 80.0, 'Protect your eyes in style with our designer sunglasses.', 4);
INSERT INTO product (id, name, price, description, category_id) VALUES (19, 'Leather Wallet', 30.0, 'Stay organized and stylish with our premium leather wallet.', 4);
INSERT INTO product (id, name, price, description, category_id) VALUES (20, 'Fashion Jewelry Set', 50.0, 'Complete your look with our stunning fashion jewelry set.', 4);
