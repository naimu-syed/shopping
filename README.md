# E-commerce Application Documentation

## Overview
This documentation provides an overview of the E-commerce application, including its architecture, functionalities, endpoints, and usage instructions.

### Application Description
The E-commerce application is a web-based platform designed to showcase products across various categories. Users can browse through the available products, view their details, and find similar products within the same category.

## Architecture
The application follows a client-server architecture, where the client interacts with the backend server through RESTful API endpoints. The frontend is built using HTML, CSS, and JavaScript, while the backend is implemented using the Spring Boot framework. Data storage is managed using a MySQL database.

### Frontend
The frontend of the application is responsible for presenting the user interface and handling user interactions. It consists of HTML, CSS, and JavaScript files that render the webpages and dynamically update content based on user actions.

### Backend
The backend of the application handles business logic, data processing, and interaction with the database. It provides RESTful API endpoints for client-server communication and serves data to the frontend. The backend is implemented using Spring Boot, which offers a robust framework for building Java-based web applications.

### Database
The application utilizes a MySQL database to store product and category information. Tables are structured to represent categories, products, and their relationships. SQL scripts are provided to create and populate the necessary tables with sample data.

## Functionality
The E-commerce application offers the following key functionalities:

1. **List Products**: Users can view a list of all available products categorized by their respective categories.

2. **Select Product**: Upon selecting a product, the application displays similar products within the same category.

## Endpoints
The following RESTful API endpoints are available in the backend:

- **GET /api/category/get/all**: Fetch all categories and their products.
- **GET /api/product/get/similar/{productId}**: Fetch similar products based on the provided product ID.
- **Swagger Documentation**: Visit [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) to access the Swagger documentation for detailed API information.

## Usage
To use the E-commerce application, follow these steps:

1. **Start the Backend Server**: Run the Spring Boot application to start the backend server. Ensure that the server is running and accessible.

2. **Access the Webpage**: Open [localhost:8080](http://localhost:8080/) in a web browser. This will load the frontend of the application.

3. **Browse Products**: The webpage will display a list of available products categorized by their respective categories. Users can scroll through the list to view products.

4. **Select Product**: Click on a product to select it. The application will dynamically load and display similar products within the same category below the selected product.

## Conclusion
The E-commerce application demonstrates the integration of frontend and backend technologies to create a responsive and interactive web platform for showcasing products. By following the provided documentation, users can effectively navigate the application and explore its features.

For any further assistance or inquiries, please contact the application developer.

**Developer Contact Information:**
Naimuddin Syed
naimusyed72@gmail.com

**Note**: Ensure that the SQL data script provided in the resources directory is executed to populate the database with sample data.